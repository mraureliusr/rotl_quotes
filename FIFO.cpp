/*
 * File:   FIFO.cpp
 * Author: amr
 *
 * Created on October 1, 2020, 4:58 PM
 */

#include "FIFO.hpp"


FIFO::FIFO(int size) {
    fifoBuffer = (uint8_t *)malloc(sizeof(uint8_t) * size); // create a 64-byte buffer
    fifoSize = size;
    readIndex = 0;
    writeIndex = 0;
    usedSize = 0;
}

uint8_t FIFO::read(void) {
    uint8_t retValue = 0;
    if(usedSize == 0) {
        return 0;
    }
    retValue = fifoBuffer[readIndex];
    readIndex++;
    if(readIndex == fifoSize) {
        readIndex = 0;
    }
    usedSize--;
    return retValue;
}

uint8_t FIFO::write(uint8_t item) {
    if(usedSize >= fifoSize) {
        return 0;
    }
    fifoBuffer[writeIndex] = item;
    writeIndex++;
    if(writeIndex == fifoSize) {
        writeIndex = 0;
    }
    usedSize++;
    return 1;
}