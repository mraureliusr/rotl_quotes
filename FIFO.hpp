/*
 * File:   FIFO.hpp
 * Author: amr
 *
 * Created on October 1, 2020, 4:58 PM
 */

#ifndef FIFO_HPP
#define	FIFO_HPP

#include <stdint.h>
#include <stdlib.h>

class FIFO {
  public:
    FIFO(int size);
    uint8_t read(void);
    uint8_t write(uint8_t item);
    int fifoSize = 0;
    int readIndex = 0;
    int writeIndex = 0;
    int usedSize = 0;
    uint8_t *fifoBuffer;
  private:

};

#endif	/* FIFO_HPP */

