/*
 * File:   FlashStorage.hpp
 * Author: amr
 *
 * Created on October 1, 2020, 5:03 PM
 */

#ifndef FLASHSTORAGE_HPP
#define	FLASHSTORAGE_HPP

#include "FIFO.hpp"
#include "UART.hpp"
#include <xc.h>
#include <stdint.h>
#include <stdlib.h>

#define FLASH_OKAY 0
#define FLASH_BUSY 1
#define FLASH_PROG_ERROR 2
#define FLASH_ERASE_ERROR 3

class FlashStorage {
  public:
    FlashStorage(FIFO *fifobuffer, UART *serialBus);
    uint8_t readStatusRegister(void);
    void sectorErase(uint8_t sector);
    uint8_t read(uint32_t addr, uint32_t length);
    uint8_t write(uint32_t addr, uint32_t length, uint8_t *data);
    uint8_t program(void);
  private:
    FIFO *buffer;
    UART *serial;
    void enableFlashWrite(void);

};

#endif	/* FLASHSTORAGE_HPP */

