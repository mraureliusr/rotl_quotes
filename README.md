# ROTL Quotes

## Intro
This is just a silly little project that I thought up a few weeks ago.
It's based on a PIC32MX270F128D microcontroller, alongside an MCP4725
I2C DAC, and a S25FL512S 512Mbit Flash chip for storage.

## Technical details
In my initial attempt to keep the project simple, I kind of shot myself
in the foot. There's no need to keep it simple, as a lot of the code
I write for this project can be re-used, so I may as well do it the
right way. So I put everything into classes, and abstracted as much
as I am comfortable with.

The PIC32 reads audio samples from the flash, and then pipes them
to the MCP4725. It also bit-shifts them to the left because the 
DAC is a 12-bit DAC but the samples are all 8-bit. I chose to use
8-bit samples because originally I was using an 8-bit DAC, and the
flash is an 8-bit flash. To have 12 bits per sample would require
using 2 bytes per sample, doubling the storage space required for
only a minor increase in fidelity. If I want better fidelity (and
after testing, I might) the easiest thing to do would be to increase
the sample rate from 8kHz to 16kHz or so. That would double the data
stored but at a great increase of fidelity. But, since this is all
just speech data that will probably be output through a crappy little
speaker, it doesn't seem like a big deal (yet).

## Contributing
You are welcome to contribute via pull requests and issues, if you find
any. Please submit your code formatted with astyle using the following
options:

`astyle -n -z2 -Z -A14 *.cpp *.hpp`

Code that isn't formatted correctly won't be merged! Just add that astyle
command as a post-build step, or if you have to write code with a different
style, just redirect the output of the astyle command to another folder
which is connected to your fork, or something.

You must also release your contributions under the terms of the Mozilla
Public License.

## Copyright
![Licenses](https://frozenelectronics.ca/licenses_small.png)

All code, documentation, and design files are released under open-source licenses.  
All of the code is under [MPL v2.0](https://opensource.org/licenses/MPL-2.0).  
All of the documentation is under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.txt).  
All of the hardware design files are under [CERN OHL 2.0](https://kt.cern/ohlv2).  
