/*
 * File:   UART.cpp
 * Author: amr
 *
 * Created on October 2, 2020, 5:51 PM
 */

#include "UART.hpp"

UART::UART(uint32_t baud, uint32_t clockSpeed) {
    double cbaud = 0;
    uint16_t ibaud = 0;
    cbaud = ((double)clockSpeed/((double)baud*16.0)) - 1.0;
    ibaud = (uint16_t)(cbaud - 0.5);

    U1BRG = ibaud;
    U1STAbits.UTXEN = 1; // enable transmitter
    U1STAbits.URXEN = 1; // enable receiver
    U1MODEbits.ON = 1; // enable UART
}

void UART::send(uint8_t c) {
    U1TXREG = c;
    while(!U1STAbits.TRMT); // wait for transmit
    return;
}

uint8_t UART::receive(void) {
    uint8_t c;
    while(!(U1STAbits.URXDA)) ; // wait for a character
    c = U1RXREG;
    return c;
}