/*
 * File:   UART.hpp
 * Author: amr
 *
 * Created on October 2, 2020, 5:51 PM
 */

#ifndef UART_HPP
#define	UART_HPP
#include <stdint.h>
#include <stdlib.h>
#include <xc.h>
class UART {
  public:
    UART(uint32_t baud, uint32_t clockSpeed);
    void send(uint8_t c);
    uint8_t receive(void);
  private:

};

#endif	/* UART_HPP */

