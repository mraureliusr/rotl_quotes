/** @file i2c.h
 * File:   myI2C.h
 * Author: Alexander Rowsell
 *
 * Created on May 3, 2015, 1:06 AM
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef MYI2C_H
#define MYI2C_H

#include <xc.h>

enum transactMode {
    SINGLE_SEND = 0,
    MULTIPLE_SEND = 1,
    SINGLE_RECV = 2,
    MULTIPLE_RECV = 3,
    TEST_MODE = 4
};

enum statusCodes {
    SUCCESS = 0,
    ADDR_NACK = 1,
    DATA_NACK = 2,
    NULL_ERROR = 127
};

/**
 * @class i2c
 * @brief The i2c class, controls all i2c comms
 *
 * This class is an abstraction of the I2C peripheral
 * inside the PIC32MX1xx/2xx family of devices.
 * Simply create an instance of this class, and then
 * use the transact() function to send and receive
 * data over the I2C bus. Colours are excellent
 */
class i2c {
  public:
    i2c(uint32_t speed, uint32_t pclk);

    uint8_t transact(uint8_t addr, uint8_t *send, \
                     uint8_t *recv, uint8_t size, transactMode mode, \
                     uint16_t *memAddr = NULL);
};


#endif
