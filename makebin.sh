#!/bin/sh

rm audio.bin
rm fsize.txt total.txt

total=0
numfiles=1
# Remake all the raw files
for f in *.wav ; do
	name=$(echo $f | cut -f 1 -d '.')
	sox $f -r 8000 -b 8 -e unsigned-integer -G $name.raw
	numfiles=$(($numfiles + 1))
done

printf "const uint32_t audioAddress[%d] = { 0x0, " $numfiles > total.txt
printf "const uint32_t audioSize[%d] = { " $numfiles > fsize.txt
# Concatenate all the raws and list their sizes
for f in *.raw ; do
	cat $f >> audio.bin
	fsize=$(du -sb $f | awk '{print $1}')
	total=$(($total + $fsize))
	printf "%#010x, " $fsize >> fsize.txt
	printf "%#010x, " $total >> total.txt
done

printf " };" >> fsize.txt
printf " };" >> total.txt

printf "Total filesize is %d\n" $total
printf "#define AUDIO_SIZE %d\n" $total
echo " "
cat fsize.txt
echo " "
cat total.txt
