#!/usr/bin/env python3

import serial
import sys
def program():
    print("Starting transfer...\n")
    with open("audio.bin", "rb") as binFile:
        sport = serial.Serial(port="/dev/ttyUSB0", baudrate=19200, dsrdtr=True)
        #sport = serial.Serial(port="/dev/pts/7", baudrate=115200)
        sport.reset_input_buffer()
        count = 0
        while(dataBuffer := binFile.read(64)):
              sport.read_until(b'\x33')
              sport.write(dataBuffer)
              print(".", end="", file=sys.stdout, flush=True)

        sport.close()

    print("All done?")

if __name__ == '__main__':
    program()
